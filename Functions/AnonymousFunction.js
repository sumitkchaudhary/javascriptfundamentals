//call the function by using ()
(function () {
    console.log('Immediately invoked function execution');
})();

let person = {
    firstName: 'John',
    lastName: 'Doe'
};

(function () {
    console.log(person.firstName + ' ' + person.lastName);
})(person);

