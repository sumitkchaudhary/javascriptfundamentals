//Without return type function
function say(message){
    console.log(message);
}

//Return type of variable
say("Hello My Name is Sumit Chaudhary");

function addTwoNumber(a,b){
    return a+b;
}

console.log(addTwoNumber(10,20));