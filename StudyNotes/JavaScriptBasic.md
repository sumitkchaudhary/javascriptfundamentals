# Introduction of javascript
JavaScript is an object-based scripting language which is lightweight and cross-platform. 
JavaScript is not a compiled language, but is is a transalted language. The JavaScript translator (embeded in the browser) is responsible for translating the JavaScript code for the web browser.

JavaScript (js) is a light-weight object-oriented programming language which is used by several websites for scripting the webpages. It is an interpreted, full-fledged programming language that enables dynamic interactivity on websites when applied to an HTML document. It was introduced in the year 1995 for adding programs to the webpages in the Netscape Navigator browser. Since then, it has been adopted by all other graphical web browsers. With JavaScript, users can build modern web applications to interact directly without reloading the page every time. The traditional website uses js to provide several forms of interactivity and simplicity.

JavaScript is a scripting language that is used to create and manage dynamic web pages, basically anything that moves on your screen without requiring you to refresh your browser. It can be anything from animated graphics to an automatically generated Facebook timeline. 

# Key part of JavaScript

HTML is the structure of your page like the headers, the body text, any images you want to include. It basically defines the contents of a web page.
CSS controls how that page looks (it’s what you’ll use to customize fonts, background colors, etc.).
JavaScript is the third element. Once you’ve created your structure (HTML) and your aesthetic vibe (CSS), JavaScript makes your site dynamic (automatically updateable).

lthough, JavaScript has no connectivity with Java programming language. The name was suggested and provided in the times when Java was gaining popularity in the market. In addition to web browsers, databases such as CouchDB and MongoDB uses JavaScript as their scripting and query language.

# Features of JavaScript

There are following features of JavaScript:

1. All popular web browsers support JavaScript as they provide built-in execution environments.
2. JavaScript follows the syntax and structure of the C programming language. Thus, it is a structured programming language.
3. JavaScript is a weakly typed language, where certain types are implicitly cast (depending on the operation).
4. JavaScript is an object-oriented programming language that uses prototypes rather than using classes for inheritance.
5. It is a light-weighted and interpreted language.
6. It is a case-sensitive language.
    Case-sensitivity refers to whether a program or system can distinguish between uppercase and lowercase letters in text. For example, in a case-sensitive system, "hello" and "Hello" would be considered two different words.
7. JavaScript is supportable in several operating systems including, Windows, macOS, etc.
8. It provides good control to the users over the web browsers.

# History of JavaScript
In 1993, Mosaic, the first popular web browser, came into existence. In the year 1994, Netscape was founded by Marc Andreessen. He realized that the web needed to become more dynamic. Thus, a 'glue language' was believed to be provided to HTML to make web designing easy for designers and part-time programmers. Consequently, in 1995, the company recruited Brendan Eich intending to implement and embed Scheme programming language to the browser. But, before Brendan could start, the company merged with Sun Microsystems for adding Java into its Navigator so that it could compete with Microsoft over the web technologies and platforms. Now, two languages were there: Java and the scripting language. Further, Netscape decided to give a similar name to the scripting language as Java's. It led to 'Javascript'. Finally, in May 1995, Marc Andreessen coined the first code of Javascript named 'Mocha'. Later, the marketing team replaced the name with 'LiveScript'. But, due to trademark reasons and certain other reasons, in December 1995, the language was finally renamed to 'JavaScript'. From then, JavaScript came into existence.


# Why JavaScript?
    . JavaScript is an essential programming language, almost compulsory to learn for students or software developers that are gravitated towards web development. Wondering why? Here’s the answer:

    . Javascript is the most popular programming language in the world and that makes it a default choice for web development. There are many frameworks available which you can use to create web applications once you have learned JavaScript.
    . JavaScript offers lots of flexibility. You can create stunning and fast web applications with tons of customizations to provide users with the most relevant graphical user interface.
    . JavaScript is now also used in mobile app development, desktop app development, and game development. This opens many possibilities for you as a Javascript developer.
    . Due to the high demand in the industry, there are tons of job growth opportunities and high pay for those who know JavaScript.
    . The incredible thing about Javascript is that you can find tons of frameworks and libraries already developed, which can be used directly in web development. That reduces the development time and enhances the graphical user interface.

# What is JavaScript Used For?


![Alt text](image.png)

    . JavaScript is used in various fields from the web to servers, and here’s a quick list of the significant areas it’s used in:
    . Web Applications: JavaScript is used for adding interactivity and automation to websites. So, if you want your web application to be anything more than just a static page of contents, you’ll probably need to do some “JavaScript’ing.”
    . Mobile Applications: JavaScript isn’t just for developing web applications; it is also used for developing applications for phones and tablets. With frameworks like React Native, you can develop full-fledged mobile applications with all those fancy animations.
    . Web-based Games: If you’ve ever played a game directly on the web browser, JavaScript was probably used to make that happen.
    . Back-end Web Development: JavaScript has traditionally been used for developing the front-end parts of a web application. However, with the introduction of NodeJS, a prevalent back-end JavaScript framework, things have changed. And now, JavaScript is used for developing the back-end structure also.


# Whitespace

Whitespace refers to characters that provide the space between other characters. JavaScript has the following whitespace:

Carriage return
Space
New Line
tab

# Statements
A statement is a piece of code that either declares a variable or instructs the JavaScript engine to perform a task. A simple statement is concluded by a semicolon (;).

3 Blocks
A block is a sequence of zero or more simple statements. A block is delimited by a pair of curly brackets


# Identifiers
An identifier is a name you choose for variables, parameters, functions, classes, etc.

An identifier name starts with a letter (a-z, or A-Z), an underscore(_), or a dollar sign ($) and is followed by a sequence of characters including (a-z, A-Z), numbers (0-9), underscores (_), and dollar signs ($).

Note that the letter is not limited to the ASCII character set and may include extended ASCII or Unicode, though it is not recommended.

Identifiers in JavaScript are case-sensitive. For example, the message is different from the Message.

# Variable 

A variable is a label that references a value like a number or string. Before using a variable, you need to declare it.

# Declare a variable

To declare a variable, you use the var keyword followed by the variable name as follows:

        var message;


A variable name can be any valid identifier. By default, the message variable has a special value undefined if you have not assigned a value to it.

Variable names follow these rules:

Variable names are case-sensitive. This means that the message and Message are different variables.
Variable names can only contain letters, numbers, underscores, or dollar signs and cannot contain spaces. Also, variable names must begin with a letter, an underscore (_) or a dollar sign ($).
Variable names cannot use the reserved words.
By convention, variable names use camelcase like message, yourAge, and myName.

JavaScript is a dynamically typed language. This means that you don’t need to specify the variable’s type in the declaration like other static-typed languages such as Java or C#.

Starting in ES6, you can use the let keyword to declare a variable like this:

        let message;

It’s a good practice to use the let keyword to declare a variable. Later, you’ll learn the differences between var and let keywords. And you should not worry about it for now.


# Constants
A constant holds a value that doesn’t change. To declare a constant, you use the const keyword. When defining a constant, you need to initialize it with a value. 

# Data Type
[text](../basics/DataTypes.js)

JavaScript has the primitive data types:
JavaScript is a dynamically typed language, meaning that a variable isn’t associated with a specific type. In other words, a variable can hold a value of different types.

1. null
2. undefined
3. boolean
4. number
5. string
6. symbol – available from ES2015
7. bigint – available from ES2020

To determine the current type of the value stored in a variable, you use the typeof operator:

![alt text](image-1.png)

The typeof null returns object is a known bug in JavaScript. A proposal to fix was rejected due to the potential to break many existing sites.

Note that JavaScript automatically converts a floating-point number into an integer if the number appears to be a whole number.

The reason is that Javascript always wants to use less memory since a floating-point value uses twice as much memory as an integer value

NaN
NaN stands for Not a Number. It is a special numeric value that indicates an invalid number. For example, the division of a string by a number returns NaN:.

# Number
JavaScript Numbers
Summary: in this tutorial, you’ll learn about the JavaScript number types and how to use them effectively.

Introduction to the JavaScript Number
JavaScript uses the number type to represent both integers and floating-point values. Technically, the JavaScript number type uses the IEEE-754 format.

ES2020 introduced a new primitive type bigint representing big integer numbers with values larger than 253 – 1.

To support various types of numbers, JavaScript uses different number literal formats.

Integer numbers
The following shows how to declare a variable that holds a decimal integer:

let counter = 100;
Code language: JavaScript (javascript)
Integers can be represented in the following formats:

Octal (base 8)
Hexadecimal (based 16)
When you use the octal and hexadecimal numbers in arithmetic operations, JavaScript treats them as decimal numbers.

Octal numbers
An octal literal number starts with the digit zero (0) followed by a sequence of octal digits (numbers from 0 through 7). For example:

let num = 071;
console.log(num);
Code language: JavaScript (javascript)
Output:

57
If an octal number contains a number not in the range from 0 to 7, the JavaScript engine ignores the 0 and treats the number as a decimal. For example:

let num = 080;
console.log(num);
Code language: JavaScript (javascript)
Output:

80
This implicit behavior might cause issues. Therefore, ES6 introduced a new octal literal that starts with the 0o followed by a sequence of octal digits (from 0 to 7). For example:

let num = 0o71;
console.log(num);
Code language: JavaScript (javascript)
Output:

57
If you have an invalid number after 0o, JavaScript will issue a syntax error like this:

let num = 0o80;
console.log(num);
Code language: JavaScript (javascript)
Output:

let num = 0o80;
          ^^
SyntaxError: Invalid or unexpected token
Code language: JavaScript (javascript)
Hexadecimal numbers
Hexadecimal numbers start with 0x or 0X followed by any number of hexadecimal digits (0 through 9, and a through f). For example:

let num = 0x1a;
console.log(num);
Code language: JavaScript (javascript)
Output:

26
Floating-point numbers
To define a floating-point literal number, you include a decimal point and at least one number after that. For example:

let price = 9.99;
let tax = 0.08;
let discount = .05; // valid but not recommeded
Code language: JavaScript (javascript)
When you have a very big number, you can use e-notation. E-notation indicates a number should be multiplied by 10 raised to a given power. For example:

let amount = 3.14e7;
console.log(amount);
Code language: JavaScript (javascript)
Output:

31400000
The notation 3.14e7 means that take 3.14 and multiply it by 107.

Likewise, you can use the E-notation to represent a very small number. For example:

let amount = 5e-7; 
console.log(amount);
Code language: JavaScript (javascript)
Output:

0.0000005
Code language: CSS (css)
The 5e-7 notation means that take 5 and divide it by 10,000,000.

Also, JavaScript automatically converts any floating-point number with at least six zeros after the decimal point into e-notation. For example:

let amount = 0.0000005;
console.log(amount);
Code language: JavaScript (javascript)
Output:

5e-7
Floating-point numbers are accurate up to 17 decimal places. When you perform arithmetic operations on floating-point numbers, you often get the approximate result. For example:

let amount = 0.2 + 0.1;
console.log(amount);
Code language: JavaScript (javascript)
Output:

0.30000000000000004
Code language: CSS (css)
Big Integers
JavaScript introduced the bigint type starting in ES2022. The bigint type stores whole numbers whose values are greater than 253 – 1.

A big integer literal has the n character at the end of an integer literal like this:

let pageView = 9007199254740991n;


# JavaScript Numeric Separator

Summary: in this tutorial, you’ll learn how to use the JavaScript numeric separator to make the numeric literals more readable.

Introduction to the JavaScript numeric separator
The numeric separator allows you to create a visual separation between groups of digits by using underscores (_) as separators.

For example, the following number is challenging to read especially when it contains long digit repetitions:

const budget = 1000000000;
Code language: JavaScript (javascript)
Is this a billion or a hundred million?

The numeric separator fixes this readability issue as follows:

const budget = 1_000_000_000;
Code language: JavaScript (javascript)
As you can see, the number is now very easy to interpret.

JavaScript allows you to use numeric separators for both integer and floating-point numbers. For example:

let amount = 120_201_123.05; // 120201123.05
let expense = 123_450; // 123450
let fee = 12345_00; // 1234500
Code language: JavaScript (javascript)
It’s important to note that all numbers in JavaScript are floating-point numbers.

Also, you can use the numeric separators for factional and exponent parts. For example:

let amount = 0.000_001; // 1 millionth
Code language: JavaScript (javascript)
It’s important to notice that you can use the numeric separator for bigint literal, binary literal, octal literal, and hex literal. For example:

// BigInt
const max = 9_223_372_036_854_775_807n;

// binary
let nibbles = 0b1011_0101_0101;

// octal
let val = 0o1234_5670;

// hex
let message = 0xD0_E0_F0;
Code language: JavaScript (javascript)
Summary
Use underscores (_) as the numeric separators to create a visual separation between groups of digits.