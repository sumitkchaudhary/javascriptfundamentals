let eligible = false,
    required = true;

console.log(!eligible);
console.log(!required);

console.log(!undefined); // true
console.log(!null); // true
console.log(!20); //false
console.log(!0); //true
console.log(!NaN); //true
console.log(!{}); // false
console.log(!''); //true
console.log(!'OK'); //false
console.log(!false); //true
console.log(!true); //false

let counter = 10;
console.log(!!counter); // true

//&& Operator
/**
 *  a	    b	    a && b
    true	true	true
    true	false	false
    false	true	false
    false	false	false
 */
let a=true; let b=true;
// true	true	true
console.log(a && b);

b = false;
//true	false	false
console.log(a && b);
//false	true	false
a=false; b=true;
console.log(a && b);

b=false;
//false	false	false
console.log(a && b);


//|| Operator
/**
 *  x	    x	    x && y
    true	true	true
    true	false	true
    false	true	true
    false	false	false
 */
    let x=true; let y=true;
    // true	true	true
    console.log(x || y);
    
   // true	false	true
   console.log(x || y); 
   // false	true	true
   console.log(x || y);
    //false	false	false
    console.log(x || y);