let a=40;
let b=10;
console.log("a has value of "+a);
console.log("b has value of "+b);

a=b;
console.log("After assign the value of b to a "+a);

a+=b;
console.log("Assigns the result of a plus b to a. "+a);

a-=b;
console.log("Assigns the result of a minus b to a. "+a);

a*=b;
console.log("Assigns the result of a times b to a. "+a);

a/=b;
console.log("Assigns the result of a divided b to a. "+a);

a %= b;

console.log("Assigns the result of a modulo b to a. "+a);

a &=b;

console.log("Assigns the result of a AND b to a. "+a);


a |=b;

console.log("Assigns the result of a OR b to a. "+a);

a ^=b;

console.log("Assigns the result of a XOR b to a. "+a);


a <<= b;

console.log("Assigns the result of a shifted left by b to a. "+a);

a >>= b

console.log("Assigns the result of a shifted right (sign preserved) by b to a. "+a);

a >>>= b;

console.log("Assigns the result of a shifted right by b to a. "+a);

let c = 30;
a = b = c; // all variables are 30
b = c; // b is 30
a = b; // a is also 30 