let sum = 10+20;
console.log("Addition "+sum);

let substraction = 20-10;
console.log("Substraction "+substraction);

let multiplication = 10*20;
console.log("Multiplication "+multiplication);

let devision = 20/10;
console.log("Devision "+devision);


let Remainder  = 21%10;
console.log("Remainder  "+Remainder);

/**
 * If one value is a string, it implicitly converts the numeric value into a string and concatenates two strings.
 */
let oneNumericOneString = 10 + '20';

console.log(oneNumericOneString);


//If both values are strings, it concatenates the second string to the first one.

let bothString = '50' + '20';

console.log(bothString);

let energy = {
    valueOf() {
      return 100;
    },
  };
  
  let currentEnergy = energy - 10;
  console.log(currentEnergy);
  
  currentEnergy = energy + 100;
  console.log(currentEnergy);
  
  currentEnergy = energy / 2;
  console.log(currentEnergy);
  
  currentEnergy = energy * 1.5;
  console.log(currentEnergy);

  let secondEnergy = {
    toString() {
      return 50;
    },
  };
  
  let secondcurrentEnergy = secondEnergy - 10;
  console.log(secondcurrentEnergy);
  
  secondcurrentEnergy = secondEnergy + 100;
  console.log(secondcurrentEnergy);
  
  secondcurrentEnergy = secondEnergy / 2;
  console.log(secondcurrentEnergy);
  
  secondcurrentEnergy = secondEnergy * 1.5;
  console.log(secondcurrentEnergy);
  




