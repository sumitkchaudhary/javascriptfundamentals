let x=10;
let y=+x;
console.log("Convert a value into a number "+y);

 y=-x;
console.log("Convert a value into a number and negative it "+y);

y=++x;
console.log("Add one to the value  (Prefix)"+y);

y=--x;
console.log("Subtract one to the value (Prefix) "+y);


y=x++;
console.log("Add one to the value (Postfix) "+y);

y=x--;
console.log("Subtract one to the value (Postfix) "+y);


let person = {
    name: 'John',
    toString: function () {
      return '25';
    },
  };
  
  console.log(+person);