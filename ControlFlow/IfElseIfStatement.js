let month=7;
let monthName;

if(month==1){
    monthName='January';
}else if(month ==2){
    monthName='February';
}else if(month ==3){
    monthName='March';
}else if(month ==4){
    monthName='April';
}else if(month ==5){
    monthName='May';
}else if(month ==6){
    monthName='June';
}else if(month ==7){
    monthName='July';
}else if(month ==8){
    monthName='August';
}else if(month ==9){
    monthName='September';
}else if(month ==10){
    monthName='October';
}else if(month ==11){
    monthName='November';
}else if(month ==12){
    monthName='December';
}else {
    monthName='Invalid Month';
}

console.log(monthName);

let weight = 70; // kg
let height = 1.72; // meter

// calculate the body mass index (BMI)
let bmi = weight / (height * height);

let weightStatus;

if (bmi < 18.5) {
  weightStatus = 'Underweight';
} else if (bmi >= 18.5 && bmi <= 24.9) {
  weightStatus = 'Healthy Weight';
} else if (bmi >= 25 && bmi <= 29.9) {
  weightStatus = 'Overweight';
} else {
  weightStatus = 'Obesity';
}

console.log(weightStatus);