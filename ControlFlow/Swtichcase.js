let monthNumber=15;
let monthName;
//Years
switch(monthNumber){
    case 1:
        monthName="January";
        break;
    case 2:
        monthName="February";
        break;
    case 3:
        monthName="March";
        break;
    case 4:
        monthName="April";
        break; 
    case 5:
        monthName="May";
        break;
    case 6:
        monthName="June";
        break;
    case 7:
        monthName="July";
        break;
    case 8:
        monthName="August";
        break;
    case 9:
        monthName="September";
        break; 
    case 10:
        monthName="October";
        break;
    case 11:
        monthName="November";
        break
    case 12:
        monthName="December";
        break;
    default:
        monthName="Please provide correct month between 1 to 12";
}

console.log(monthName);


let weekdayName="Satuday";
let weekday;

switch(weekdayName){
    case "Monday":
        weekday=1;
        break;
    case "Tuesday":
        weekday=2;
        break;
    case "Wednesday":
        weekday=3;
        break;
    case "Thursday":
        weekday=4;
        break;
    case "Friday":
        weekday=5;
        break;
    case "Saturday":
        weekday=6;
        break;
    case "Sunday":
        weekday=7;
        break;
    default:
        weekday="Please provide correct weekday name so that you will get the day";
}

console.log(weekday);

let year = 2016;
let month = 2;
let dayCount;

switch (month) {
  case 1:
  case 3:
  case 5:
  case 7:
  case 8:
  case 10:
  case 12:
    dayCount = 31;
    break;
  case 4:
  case 6:
  case 9:
  case 11:
    dayCount = 30;
    break;
  case 2:
    // leap year
    if ((year % 4 == 0 && !(year % 100 == 0)) || year % 400 == 0) {
      dayCount = 29;
    } else {
      dayCount = 28;
    }
    break;
  default:
    dayCount = -1; // invalid month
}

console.log(dayCount); // 29