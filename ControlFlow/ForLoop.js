//Counting
//for(let i=1; i<=100; i++){
    //console.log(i);
//}


//Table

let tableFor=2;

for(let number=1; number<=10; number++){
    let result=tableFor*number;
    console.log(tableFor+" X "+number+" = "+result);
}

//Fetch the element of the array
let studentName=["Sumit", "Amit","Sachin"];

for(let i=0; i<studentName.length; i++){
    console.log(studentName[i]);
}

//Using the JavaScript for loop statement without any expression
let j = 1;
for (;;) {
  if (j > 10) {
    break;
  }
  console.log(j);
  j += 2;
}

//for loop without the loop body
let sum = 0;
for (let i = 0; i <= 9; i++, sum += i);
console.log(sum);