let number = 12345; 
console.log(typeof number);
number =12.5
console.log(typeof number);

let string = "Hello World";
console.log(typeof string);
let boolean = true;
console.log(typeof boolean);
let nullType = null;
console.log(typeof nullType);

console.log("Minimum Value of number: "+Number.MIN_VALUE);//5e-324

console.log("Maximum Value of number: "+Number.MAX_VALUE);//1.7976931348623157e+308
/*
The typeof null returns object is a known bug in JavaScript. A proposal to fix was rejected due to the potential to break many existing sites.
*/


console.log('a'/2)//NaN

