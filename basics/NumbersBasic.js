/**
 * ntegers can be represented in the following formats:

Octal (base 8)
Hexadecimal (based 16)
When you use the octal and hexadecimal numbers in arithmetic operations, JavaScript treats them as decimal numbers.

Octal numbers
An octal literal number starts with the digit zero (0) followed by a sequence of octal digits (numbers from 0 through 7). For example:
 */

//let octalNum = 071;
//console.log(octalNum);

/**
 * If an octal number contains a number not in the range from 0 to 7, the JavaScript engine ignores the 0 and treats the number as a decimal. For example:
 */
//let octalSecondNum = 080;
//console.log(octalSecondNum);
/**
 * This implicit behavior might cause issues. Therefore, ES6 introduced a new octal literal that starts with the 0o followed by a sequence of octal digits (from 0 to 7). For example:
 */

let octalThiredNum = 0o71;
console.log(octalThiredNum);

/**
 * If you have an invalid number after 0o, JavaScript will issue a syntax error like this:
 */
//let octalFourthnum = 0o80;
//console.log(octalFourthnum);

/**
 * Hexadecimal numbers
Hexadecimal numbers start with 0x or 0X followed by any number of hexadecimal digits (0 through 9, and a through f). For example:
 */

let Hexnum = 0x1a;
console.log("Hexa number "+Hexnum);

/**
 * Floating-point numbers
To define a floating-point literal number, you include a decimal point and at least one number after that. For example:
 */

let price = 9.99;
let tax = 0.08;
let discount = .05; // valid but not recommeded

/**
 * When you have a very big number, you can use e-notation. E-notation indicates a number should be multiplied by 10 raised to a given power. For example:
 */

let amount = 3.14e7;
console.log(amount);

/**
 * Big Integers
JavaScript introduced the bigint type starting in ES2022. The bigint type stores whole numbers whose values are greater than 253 – 1.

A big integer literal has the n character at the end of an integer literal like this:
 */