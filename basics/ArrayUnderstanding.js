let items=[1,2,3];

console.log("Lenght of array or elements in the array is: "+items.length) //Get the array length

console.log("Elements of the array using for loop");

for(let i=0; i<items.length; i++){
    console.log(items[i])
}


console.log("Elements of the array using for each loop");
for(let item of items){
    console.log(item)
}


let scores = new Array(9,10,3, 'Red');

let mountains = ['Everest', 'Fuji', 'Nanga Parbat'];

console.log(mountains[0]); // 'Everest'

//To change the value of an element, you assign that value to the element like this:

mountains[2] = 'K2';

console.log(mountains);

console.log(mountains.length)

//Add an element on existing array
mountains.push("Kanchanjanga")
console.log(mountains);

console.log(mountains.length)
//Adding an element in the begining of an array
let seas = ['Black Sea', 'Caribbean Sea', 'North Sea', 'Baltic Sea'];
seas.unshift('Red Sea');

console.log(seas);

//Removing an element from an array
const lastElement = seas.pop();
console.log(lastElement); 

//Removing an element from the beginning of an array

const firstElement = seas.shift();

console.log(firstElement);

//Finding an index of an element in the array

let index = seas.indexOf('North Sea');

console.log(index); // 2

//Check if a value is an array

console.log(Array.isArray(seas)); // true