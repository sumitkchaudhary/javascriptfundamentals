let firstName= 'Sumit';
let secondName= " Kumar";

let concatenate=firstName+secondName;
console.log(concatenate);

let backTickString= 'Sumit`';
console.log(backTickString);

let introduction= "I'm Sumit";
console.log(introduction);

//string interpolation.

let interpolation= `I'm ${firstName}.`;

console.log(interpolation);
//length of the string
console.log(interpolation.length);

console.log("First character of the string is : "+interpolation[0]);
console.log("Last character of the string is : "+interpolation.length-1);

//Conver on-string value to string

let booleanValue=true;

let booleanToStr=booleanValue.toString();
console.log(booleanToStr);

let backToBoolean=Boolean(booleanToStr);
console.log(backToBoolean);

/**
 * Compare String 
 * To compare two strings, you use comparison operators such as >, >=, <, <=, and == operators.
 */


let compareString= "A"=="B";
console.log(compareString);

