let a,
b;

function divide(a,b){
    if(b==0){

        throw "division by zero"  //throw keyword help to handle exception.
    }
    return a/b;
}

let result=divide(10,0);
console.log(result);