//Define empty object
let emptyObject={};

//Assigne multiple value in an object
let personDetail= {
    firstName : "Sumit",
    lastName : "Kumar",
};

//access the object name
console.log(personDetail.firstName);
console.log(personDetail.lastName);

console.log(personDetail['firstName']);
console.log(personDetail['lastName']);
//Property contains space
let address = {
    'building no': 3960,
    street: 'North 1st street',
    state: 'CA',
    country: 'USA'
};

console.log(address["building no"]);

//console.log(address.building no);

//Modify the value of property

personDetail.firstName="Amit"

console.log(personDetail.firstName);
console.log(personDetail);

//Adding new property in existing

personDetail.age=25;

console.log(personDetail);


//Deleting the property

delete personDetail.age

console.log(personDetail);


//Check if the property is exist or not

console.log('firstName' in personDetail);

console.log('age' in personDetail);
